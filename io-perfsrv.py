"""
io-PerfServer - serves HTTP(S), FTP and e-mail for performance testing purposes.

Todo:
- SMTP server (https://twistedmatrix.com/documents/current/mail/examples/)
- POP3 server (twisted.mail.pop3.POP3)
- Web proxy (https://twistedmatrix.com/documents/current/web/examples/)

Author: Vlad Topan (vtopan / gmail)
"""

import argparse
from ftplib import FTP, FTP_TLS
import logging
import os
import ssl
import sys
import time
import urllib.request

try:
    import twisted
    from twisted.internet import reactor, endpoints
    from twisted.web import static, server as tweb_server
    from twisted.web.resource import Resource
    from twisted.protocols.ftp import FTPFactory, FTPRealm
    from twisted.cred.portal import Portal
    from twisted.cred.checkers import AllowAnonymousAccess, FilePasswordDB
except ImportError:
    twisted = None

try:
    import OpenSSL
    from OpenSSL import crypto, SSL
    if twisted:
        from twisted.internet import ssl as twisted_ssl
except ImportError:
    # sys.stderr.write('[!] The pyOpenSSL library is missing, TLS not available - run `pip3 install -r requirements.txt`\n')
    crypto = SSL = twisted_ssl = None

try:
    import pyftpdlib
    from pyftpdlib.servers import FTPServer
    from pyftpdlib.authorizers import DummyAuthorizer
    from pyftpdlib.handlers import TLS_FTPHandler
except ImportError:
    pyftpdlib = None


VER = '0.1.0.20211119'
CMD = os.path.basename(sys.argv[0])
MODES = ('http', 'ftp',)



class StopWebserver(Resource):
    """
    Insta-stops the webserver when /stop is requested.
    """
    isLeaf = True

    def render(self, request):
        # todo: log timing
        reactor.stop()
        return ''



def server_http():
    fun, args = reactor.listenTCP, []
    if CFG.tls:
        if not os.path.isfile(CFG.tls_key_file):
            sys.exit(f'[!] TLS key/cert missing, run `{CMD} -gk`')
        if not CFG.port:
            CFG.port = 443
        fun = reactor.listenSSL
        args = [twisted_ssl.DefaultOpenSSLContextFactory(CFG.tls_key_file, CFG.tls_crt_file)]
    elif not CFG.port:
        CFG.port = 80
    root = static.File(CFG.path)
    root.putChild(b"stop", StopWebserver())
    site = tweb_server.Site(root)
    fun(CFG.port, site, *args)
    reactor.run()


def server_ftp():
    CFG.port = CFG.port or 21
    if CFG.engine == 'pyftpdlib' or CFG.tls:
        if not pyftpdlib:
            sys.exit('The pyftpdlib library is missing - run `pip3 install -r requirements.txt`')
        authorizer = DummyAuthorizer()
        authorizer.add_anonymous(CFG.path)
        handler = TLS_FTPHandler
        handler.certfile = CFG.tls_crt_file
        handler.keyfile = CFG.tls_key_file
        handler.authorizer = authorizer
        handler.tls_control_required = True
        handler.tls_data_required = True
        server = FTPServer((CFG.host, CFG.port), handler)
        server.serve_forever()
    else:
        p = Portal(FTPRealm(CFG.path), [AllowAnonymousAccess()])
        f = FTPFactory(p)
        reactor.listenTCP(CFG.port, f)
        reactor.run()


def server():
    if not twisted:
        sys.exit('The twisted library is missing - run `pip3 install -r requirements.txt`')
    if not CFG.host:
        CFG.host = '0.0.0.0'
    globals()[f'server_{CFG.server}']()


def client_http():
    if not os.path.isfile(CFG.filelist):
        sys.exit(f'[*] {CFG.filelist} is missing, run `{CMD} -gf` to generate')
    if not CFG.port:
        CFG.port = 443 if CFG.tls else 80
    base_url = f'http{"s" * int(CFG.tls)}://{CFG.host}:{CFG.port}/'
    total = 0
    for line in open(CFG.filelist):
        path = line.strip()
        if not path:
            continue
        t0 = time.time()
        res = urllib.request.urlopen(f'{base_url}{path}').read()
        total += time.time() - t0
        # print(f'Got {len(res)} bytes.')
    print(f'[*] HTTP download took {total:.3f}s')


def client_ftp():
    CFG.port = CFG.port or 21
    ftp = FTP_TLS() if CFG.tls else FTP()
    ftp.connect(CFG.host, CFG.port)
    ftp.login()
    if CFG.tls:
        ftp.prot_p()
    total = 0
    for line in open(CFG.filelist):
        path = line.strip()
        if not path:
            continue
        t0 = time.time()
        size = []
        ftp.retrbinary(f'RETR {path}', lambda x:size.append(len(x)))
        total += time.time() - t0
        # print(f'Got {sum(size)} bytes.')
    print(f'[*] FTP download took {total:.3f}s')



def client():
    if not CFG.host:
        CFG.host = '127.0.0.1'
    globals()[f'client_{CFG.client}']()


def generate_keys():
    if not os.path.isdir(CFG.tls_path):
        os.makedirs(CFG.tls_path)
    for fn in (CFG.tls_key_file, CFG.tls_crt_file):
        if os.path.isfile(fn):
            print(f'[*] Removing {fn}...')
            os.remove(fn)
    # generate key
    pk = crypto.PKey()
    pk.generate_key(crypto.TYPE_RSA, 1024)
    # generate (self-signed) certificate
    cert = crypto.X509()
    cert.get_subject().C = "XX"
    cert.get_subject().ST = "State"
    cert.get_subject().L = "Location"
    cert.get_subject().O = "Org"
    cert.get_subject().OU = "OrgUnit"
    cert.get_subject().CN = 'ips.server'
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(20 * 365 * 24 * 3600)
    cert.set_issuer(cert.get_subject())
    cert.set_pubkey(pk)
    cert.sign(pk, 'sha1')
    print(f'[*] Writing {CFG.tls_crt_file}...')
    open(CFG.tls_crt_file, 'wb').write(crypto.dump_certificate(crypto.FILETYPE_PEM, cert))
    print(f'[*] Writing {CFG.tls_key_file}...')
    open(CFG.tls_key_file, 'wb').write(crypto.dump_privatekey(crypto.FILETYPE_PEM, pk))


def generate_filelist():
    if not os.path.isdir(CFG.path):
        sys.exit(f'Folder {CFG.path} does not exist!')
    h = open(CFG.filelist, 'w')
    CFG.path = CFG.path.strip('/\\')
    cnt = size = 0
    for root, dirs, files in os.walk(CFG.path):
        for e in files:
            fn = f'{root}/{e}'.replace('\\', '/')
            cnt += 1
            size += os.path.getsize(fn)
            h.write(fn[len(CFG.path) + 1:] + '\n')
    h.close()
    print(f'[*] {CFG.filelist} generated ({cnt} files, {size / (1024 * 1024):.1f} MB).')


if __name__ == '__main__':
    argp = argparse.ArgumentParser(description=__doc__)
    op = argp.add_mutually_exclusive_group(required=True)
    op.add_argument('-s', '--server', help='server mode (arg: how to serve)', choices=sorted(MODES))
    op.add_argument('-c', '--client', help='client mode (arg: how to get)', choices=sorted(MODES))
    op.add_argument('-gk', '--generate-keys', help='generate TLS certificates (requires openssl command)',
            action='store_true')
    op.add_argument('-gf', '--generate-filelist', help='generate filelist (based on path)',
            action='store_true')
    argp.add_argument('-e', '--engine', help='server engine (some combinations of engine and mode don\'t work',
            choices=('twisted', 'pyftpdlib'))
    argp.add_argument('-H', '--host', help='host (default 0.0.0.0 in server mode, 127.0.0.1 for client)')
    argp.add_argument('-P', '--port', help='port (default depends on protocol & TLS)', type=int)
    argp.add_argument('-t', '--tls', help='enable TLS encryption', action='store_true')
    argp.add_argument('-tp', '--tls-path', help='TLS key/cert path (generate with -gk)',
            default='tls')
    argp.add_argument('-tk', '--tls-key-file', help='TLS key filename (in tls-path)',
            default='ips.key')
    argp.add_argument('-tc', '--tls-crt-file', help='TLS certificate filename (in tls-path)',
            default='ips.crt')
    argp.add_argument('-p', '--path', help='path to files to serve', default='files')
    argp.add_argument('-fl', '--filelist', help='file list filename', default='file.list')
    argp.add_argument('-vt', '--verify-tls', help='actually verify server TLS certificate',
            action='store_true')
    args = CFG = argp.parse_args()

    logging.basicConfig(filename=f'{CMD}.log', level=logging.ERROR)     # pyftpdlib is verbose otherwise
    if not args.verify_tls:
        ssl._create_default_https_context = ssl._create_unverified_context
    CFG.tls_key_file = os.path.join(CFG.tls_path, CFG.tls_key_file)
    CFG.tls_crt_file = os.path.join(CFG.tls_path, CFG.tls_crt_file)
    if (args.tls or args.generate_keys) and not SSL:
        sys.exit('[!] pyOpenSSL missing, cannot enable TLS! Run `pip3 install -r requirements.txt`')

    for op in ('server', 'client', 'generate_keys', 'generate_filelist'):
        if getattr(args, op):
            globals()[op]()
