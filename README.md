# io-perf

Performance testing tools & snippets.


## io-socket-script.py

Loads a TCP session (from a JSON Wireshark export) and generates replayable client / server socket
(binary) scripts (or, if given a socket script (*.iss), (re)plays the conversation).

Usage:
io-socket-script.py filename.json
or
io-socket-script.py filename.iss [host]:port [count_per_thread [number_of_threads]]

The host parameter is only required for the client.

To generate a JSON dump from Wireshark filter the desired TCP stream (e.g. "tcp.stream eq 2"), then
"File" -> "Export Packet Dissections" -> "As JSON...". 


## io-perfsrv.py

Serves HTTP(S), FTP and e-mail for performance testing purposes. 
