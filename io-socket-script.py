#!/usr/bin/env python3
"""
Loads a TCP session (from a JSON Wireshark export) and generates replayable client / server socket
(binary) scripts (or, if given a socket script (*.iss), (re)plays the conversation).

Usage:
io-socket-script.py filename.json
or
io-socket-script.py filename.iss [host]:port [count_per_thread [number_of_threads]]

The host parameter is only required for the client.

To generate a JSON dump from Wireshark filter the desired TCP stream (e.g. "tcp.stream eq 2"), then
"File" -> "Export Packet Dissections" -> "As JSON...".

Script file format (*.iss):

- all integer values are encoded big-endian
- header:
    - raw b'ioSOCKSCRPT'
    - one byte encoding the version (3 currently)
    - one byte encoding flags:
        - bits 0-1: endpoint: 1 = client, 2 = server
        - bit 2: enable / force TLS
        - bit 3: UDP (if set)
    - one byte encoding the IP version (4 or 6)
    - two bytes encoding the port
    - four or sixteen bytes encoding the IP address
- sequence of blocks:
    - one byte - operation (1 = "send", 2 = "receive", 3 = "stop")
    - if the operation is "send" or "receive", four more bytes encode the payload size
    - if the operation is "send", the payload follows
- the final block must encode a "stop"

Author: Vlad Topan (vtopan / gmail)
"""
import argparse
from ipaddress import IPv4Address, IPv6Address
import json
import mmap
import os
import socket
import struct
import ssl
import sys
import threading
import time


VER = '0.1.2.20220110'
PROTO_VER = 3
MAGIC = b'ioSOCKSCRPT'
EXT = '.iss'

F_CLIENTSERVER = 3
E_CLIENT = 1
E_SERVER = 2
F_TLS = 4
F_TLS_BIT = 2
F_UDP_BIT = 4
OP_SEND = ord('S')  # 0x53
OP_RECV = ord('R')  # 0x52
OP_STOP = ord('X')  # 0x58
IPV4 = 4
IPV6 = 6
TIMES = []
TCP = 0
UDP = 1
TPROTO = ['TCP', 'UDP']
SOCK_TYPE = [socket.SOCK_STREAM, socket.SOCK_DGRAM]

OPS = {OP_SEND: 'SEND', OP_RECV: 'RECV', OP_STOP: 'STOP'}
TLS_VER = {
    '1': ssl.PROTOCOL_TLSv1,    # aka SSLv3
    '1.1': ssl.PROTOCOL_TLSv1_1,
    '1.2': ssl.PROTOCOL_TLSv1_2,
    '1.3': ssl.PROTOCOL_TLS,
    }


def dbg(msg):
    if CFG.verbose:
        print('[#] ' + msg)


def json_to_ss(filename):
    client = server = stream = None
    packets = json.load(open(sys.argv[1]))
    cnt = 0
    for pkt in packets:
        layers = pkt['_source']['layers']
        ip = layers.get('ip')
        tcp = layers.get('tcp') or {}
        udp = layers.get('udp') or {}
        if not (ip and (((not CFG.udp) and tcp) or (CFG.udp and udp))):
            continue
        srcport = tcp.get('tcp.srcport', udp.get('udp.srcport'))
        dstport = tcp.get('tcp.dstport', udp.get('udp.dstport'))
        src = (ip['ip.src'], srcport)
        dst = (ip['ip.dst'], dstport)
        udp_key = sorted([src, dst])
        if stream is None:
            # first packet
            client = src
            server = dst
            print(f'[*] Connection from {src[0]}:{src[1]} to {dst[0]}:{dst[1]}')
            if CFG.udp:
                stream = udp_key
            else:
                stream = tcp['tcp.stream']
            cout = open(filename + '.client' + EXT, 'wb')
            sout = open(filename + '.server' + EXT, 'wb')
            ipver, cls = (IPV4, IPv4Address) if ip['ip.version'] == '4' else (IPV6, IPv6Address)
            flags = (int(CFG.tls) << F_TLS_BIT) | (F_UDP_BIT if CFG.udp else 0)
            cout.write(MAGIC + struct.pack('>BBBH', PROTO_VER, flags | E_CLIENT, ipver, int(src[1]))
                + cls(src[0]).packed)
            sout.write(MAGIC + struct.pack('>BBBH', PROTO_VER, flags | E_SERVER, ipver, int(dst[1]))
                + cls(dst[0]).packed)
        payload = tcp.get('tcp.payload') or udp.get('udp.payload')
        if not payload:
            continue
        if CFG.udp:
            # assume it's the same UDP stream if the address:port ends match
            if stream != udp_key:
                continue
        else:
            if stream != tcp['tcp.stream']:
                continue
        o1, o2, d = (cout, sout, 'C->S') if src == client else (sout, cout, 'S->C')
        # print('[#]', d, payload.replace(':', ''))
        payload = bytes.fromhex(payload.replace(':', ''))
        o1.write(struct.pack('>BI', OP_SEND, len(payload)) + payload)
        o2.write(struct.pack('>BI', OP_RECV, len(payload)))
        cnt += 1
    print(f'[*] Wrote {cnt} events.')
    if cnt:
        cout.write(struct.pack('>B', OP_STOP))
        sout.write(struct.pack('>B', OP_STOP))


def play_socket_script(filename, count=1, host=None, port=None, conn=None):
    fh = open(filename, 'rb')
    scr = mmap.mmap(fh.fileno(), 0, access=mmap.ACCESS_READ)
    offs = len(MAGIC)
    iss_ver, flags, ip_ver, tcp_port = struct.unpack('>BBBH', scr[offs:offs + 5])
    if iss_ver != PROTO_VER:
        sys.exit(f'[!] Invalid ISS protocol version {iss_ver} (should be {PROTO_VER})!')
    endp = flags & F_CLIENTSERVER
    tproto = int(bool(flags & F_UDP_BIT))   # 0 = tcp, 1 = udp
    use_tls = ((flags & F_TLS) or CFG.tls) if tproto == TCP else False
    offs += 5
    addr_size, cls = (4, IPv4Address) if ip_ver == 4 else (16, IPv6Address)
    addr = scr[offs:offs + addr_size]
    start = offs + addr_size
    total = 0
    for i in range(count):
        offs = start
        dbg(f'[#] Iteration #{i}...')
        if use_tls:
            tls_opts = 0
            if CFG.tls_version == '1':
                tls_opts |= ssl.OP_NO_TLSv1_1 | ssl.OP_NO_TLSv1_2 | ssl.OP_NO_TLSv1_3
            elif CFG.tls_version == '1.1':
                tls_opts |= ssl.OP_NO_TLSv1_2 | ssl.OP_NO_TLSv1_3
            elif CFG.tls_version == '1.2':
                tls_opts |= ssl.OP_NO_TLSv1_3
            tls_ctx = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT if endp == E_CLIENT else ssl.PROTOCOL_TLS_SERVER)
            tls_ctx.options |= tls_opts
            if CFG.tls_no_dhke:
                if CFG.tls_version not in ('1', '1.1', '1.2'):
                    return sys.stderr.write(f'[!] Cannot set ciphers for TLS 1.3 - can\'t disable DH key exchange!\n')
                # disable Diffie-Hellman key exchange to allow decryption in Wireshark
                tls_ctx.set_ciphers('RSA')
        if endp == E_CLIENT:
            # client setup
            dbg(f'[#] Connecting to {TPROTO[tproto]}:{host}:{port}...')
            s = socket.socket(socket.AF_INET, SOCK_TYPE[tproto])
            if use_tls:
                tls_ctx.check_hostname = False
                tls_ctx.verify_mode = ssl.CERT_NONE
                s = tls_ctx.wrap_socket(s, server_hostname=host)
            if tproto == TCP:
                s.connect((host, port))
        else:
            # server setup
            if use_tls:
                if not os.path.isfile(CFG.tls_crt_file):
                    return sys.stderr.write('[!] TLS key / cert file missing, generate them with `io-perfsrv.py -gk`\n')
                tls_ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
                tls_ctx.load_cert_chain(certfile=CFG.tls_crt_file, keyfile=CFG.tls_key_file)
                conn = tls_ctx.wrap_socket(conn, server_side=True)
            if tproto == TCP:
                s, caddr = conn.accept()
                dbg(f'[#] Got connection from {TPROTO[tproto]}:{caddr}...')
            else:
                s = conn
        if tproto == TCP:
            print(f'[*] Encryption: {use_tls and " / ".join(str(x) for x in s.cipher())}')
        t0 = time.time()
        while 1:
            op = scr[offs]
            if CFG.verbose > 1:
                dbg(f'[#] Op: {OPS[op]}')
            if op == OP_STOP:
                break
            size = struct.unpack('>I', scr[offs + 1:offs + 5])[0]
            offs += 5
            if op == OP_RECV:
                got = 0
                while got < size:
                    if tproto == TCP:
                        got += len(s.recv(size))
                    else:
                        data, caddr = s.recvfrom(size)
                        got += len(data)
                if CFG.verbose > 1:
                    dbg(f'RECVd {got} bytes')
            elif op == OP_SEND:
                if tproto == TCP:
                    s.sendall(scr[offs:offs + size])
                else:
                    buf = scr[offs:offs + size]
                    while buf:
                        sent = s.sendto(buf, (host, port) if endp == E_CLIENT else caddr)
                        if sent >= len(buf):
                            break
                        buf = buf[sent:]
                if CFG.verbose > 1:
                    dbg(f'SENT {size} bytes')
                offs += size
            else:
                raise ValueError(f'Unknown op: {op}!')
        total += time.time() - t0
    TIMES.append(total)


argp = argparse.ArgumentParser(description=__doc__)
argp.add_argument('filename', help='file (.iss or .json)', nargs=1)
argp.add_argument('address', help='address (host:port)', nargs='?')
argp.add_argument('-T', '--threads', help='number of threads', type=int, default=1)
argp.add_argument('-t', '--tls', help='enable / force TLS encryption', action='store_true')
argp.add_argument('-c', '--count', help='count', type=int, default=1)
argp.add_argument('-U', '--udp', help='collect (all) UDP packets in JSON (instead of TCP stream)',
        action='store_true')
argp.add_argument('-tp', '--tls-path', help='TLS key/cert path (generate with -gk)',
        default='tls')
argp.add_argument('-tk', '--tls-key-file', help='TLS key filename (in tls-path)',
        default='ips.key')
argp.add_argument('-tc', '--tls-crt-file', help='TLS certificate filename (in tls-path)',
        default='ips.crt')
argp.add_argument('-tv', '--tls-version', help='force specific TLS version', type=str,
        choices=sorted(TLS_VER))
argp.add_argument('-nd', '--tls-no-dhke', help='TLS: disable Diffie-Hellman key exchange',
        action='store_true')
argp.add_argument('-v', '--verbose', help='be (more) verbose', action='count', default=0)
args = CFG = argp.parse_args()

ssl._create_default_https_context = ssl._create_unverified_context
CFG.tls_key_file = os.path.join(CFG.tls_path, CFG.tls_key_file)
CFG.tls_crt_file = os.path.join(CFG.tls_path, CFG.tls_crt_file)

for filename in CFG.filename:
    hdr = open(filename, 'rb').read(len(MAGIC) + 2)
    threads = []
    is_iss = hdr.startswith(MAGIC)
    print(f'[*] Processing {filename} (ISS: {is_iss})...')
    if is_iss:
        endpoint = 'client' if (hdr[-1] & F_CLIENTSERVER) == E_CLIENT else 'server'
        tproto = int(bool(hdr[-1] & F_UDP_BIT))
        if not CFG.address:
            sys.exit(f'[!] The address is required in replay mode!')
        host, port = CFG.address.split(':', 1)
        port = int(port)
        if (hdr[-1] & 3) == E_SERVER:
            conn = socket.socket(socket.AF_INET, SOCK_TYPE[tproto])
            host = host or '0.0.0.0'
            conn.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            conn.bind((host, port))
            if tproto == TCP:
                conn.listen()
            print(f'[*] Listening on {TPROTO[tproto]}:{host}:{port}...')
        else:
            host = host or '127.0.0.1'
            conn = None
        print(f'[*] Running in {endpoint} mode, {CFG.count} iteration(s) x {CFG.threads} threads')
        t0 = time.time()
        for i in range(CFG.threads):
            t = threading.Thread(target=play_socket_script, args=(filename, CFG.count, host, port, conn))
            threads.append(t)
            t.start()
        for t in threads:
            t.join()
        print(f'[*] Took: {sum(TIMES) * 1000:.3f}ms / {(time.time() - t0) * 1000:.3f}ms')
        print(f'[*] Per thread: ' + ' / '.join(f'{e * 1000:.3f}ms' for e in TIMES))
        if len(TIMES) != CFG.threads:
            print(f'[!] ERROR: Only {len(TIMES)} out of {CFG.threads} thread(s) completed successfully!')
    else:
        json_to_ss(filename)
